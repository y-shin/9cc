#include "9cc.h"

// x86-64の引数を保存するレジスタ https://qiita.com/FAMASoon/items/a93c1361f80bb28f895c
const char* argument_registers[6] = {"rdi", "rsi", "rdx", "rcx", "r8", "r9"};

int use_label() {
  label_id++;
  return label_id;
}

void gen_leftval(Node* node) {
  if (node->kind != ND_LOCALVAR) {
    error("代入の左辺値が変数ではありません");
  }
  println("  mov rax, rbp");
  println("  sub rax, %d", node->offset);
  println("  push rax");
}

void gen(Node* node) {
  // 即値
  if (node->kind == ND_NUM) {
    println("  push %d", node->val);
    return;
  }

  // 変数
  if (node->kind == ND_LOCALVAR) {
    gen_leftval(node);
    println("  pop rax");
    println("  mov rax, [rax]");
    println("  push rax");
    return;
  }

  //  単項&
  if (node->kind == ND_ADDR) {
    gen_leftval(node->lhs);  // 変数アドレスそのもの
    return;
  }

  //  単項*
  if (node->kind == ND_DEREF) {
    gen(node->lhs);
    println("  pop rax");
    println("  mov rax, [rax]");
    println("  push rax");
    return;
  }

  // 関数呼び出し
  if (node->kind == ND_FUNCCALL) {
    // 引数をレジスタに入れる
    for (unsigned int j = 6; j >= 1; j--) {
      if (node->funccall_arg_num >= j) {
        gen(node->funccall_args[j - 1]);
        println("  pop rax");
        println("  mov %s, rax", argument_registers[j - 1]);
      }
    }

    // rspを16の倍数に合わせる
    int is_sixteen_label = use_label();
    int call_end_label = use_label();
    println("  mov rax, rsp");
    println("  and rax, 0xF");
    println("  cmp rax, 0");
    println("  je .L%d", is_sixteen_label);
    // 16の倍数でない場合、callの前に8引く (call終了時に8増やす)
    println("  sub rsp, 8");
    printf("  call ");
    putchars(node->funccall_name, node->funccall_name_len);
    putchar('\n');
    println("  add rsp, 8");
    println("  jmp .L%d", call_end_label);
    // 16の倍数の場合
    println(".L%d:", is_sixteen_label);
    printf("  call ");
    putchars(node->funccall_name, node->funccall_name_len);
    putchar('\n');
    println(".L%d:", call_end_label);
    println("  push rax");
    return;
  }

  // 代入
  if (node->kind == ND_ASSIGN) {
    // lhsが ND_DEREF=>ND_DEREF=>...=>ND_DEREF=>ND_LOCALVAR という形であることを確認
    // while終了時点でのlhsはND_LOCALVARで、deref_numはND_DEREFの段数
    Node* lhs = node->lhs;
    int deref_num = 0;
    while (lhs->kind == ND_DEREF) {
      deref_num++;
      lhs = lhs->lhs;
    }
    gen_leftval(lhs);

    // deref_numの回数だけメモリアドレスから値を読む. 途中状態ではraxはポインタのアドレスで、最後のraxは実際の値のアドレス
    println("  pop rax");
    for (int j = 0; j < deref_num; j++) {
      println("  mov rax, [rax]");
    }
    println("  push rax");

    gen(node->rhs);

    println("  pop rdi");
    println("  pop rax");
    println("  mov [rax], rdi");
    println("  push rdi");
    return;
  }

  // 2項演算子
  // 1. 左の子を処理してスタックにpush
  // 2. 右の子を処理してスタックにpush
  // 3. それらをポップして計算を行う
  // 4. 計算結果をpush
  if (node->kind == ND_ADD ||
      node->kind == ND_SUB ||
      node->kind == ND_MUL ||
      node->kind == ND_DIV ||
      node->kind == ND_EQ ||
      node->kind == ND_NE ||
      node->kind == ND_LT ||
      node->kind == ND_LTE) {

    gen(node->lhs);
    gen(node->rhs);

    println("  pop rdi");
    println("  pop rax");
    if (node->kind == ND_ADD) {
      println("  add rax, rdi");
    } else if (node->kind == ND_SUB) {
      println("  sub rax, rdi");
    } else if (node->kind == ND_MUL) {
      println("  imul rax, rdi");
    } else if (node->kind == ND_DIV) {
      println("  cqo");
      println("  idiv rdi");
    } else if (node->kind == ND_EQ) {
      println("  cmp rax, rdi");
      println("  sete al");
      println("  movzb rax, al");
    } else if (node->kind == ND_NE) {
      println("  cmp rax, rdi");
      println("  setne al");
      println("  movzb rax, al");
    } else if (node->kind == ND_LT) {
      println("  cmp rax, rdi");
      println("  setl al");
      println("  movzb rax, al");
    } else if (node->kind == ND_LTE) {
      println("  cmp rax, rdi");
      println("  setle al");
      println("  movzb rax, al");
    }
    println("  push rax");
    return;
  }
  error("unexpected error");
}

void gen_stmt(Node* node) {
  // if
  if (node->kind == ND_IF) {
    int ifend_label = use_label();
    int else_label = use_label();

    gen(node->lhs->lhs);
    println("  pop rax");
    println("  cmp rax, 0");
    println("  je .L%d", else_label);
    gen_stmt(node->lhs->rhs);
    println("  jmp .L%d", ifend_label);
    println(".L%d:", else_label);
    gen_stmt(node->rhs);
    println(".L%d:", ifend_label);
    return;
  }

  // while
  if (node->kind == ND_WHILE) {
    int start_label = use_label();
    int end_label = use_label();

    println(".L%d:", start_label);
    gen(node->lhs);
    println("  pop rax");
    println("  cmp rax, 0");
    println("  je .L%d", end_label);
    gen_stmt(node->rhs);
    println("  jmp .L%d", start_label);
    println(".L%d:", end_label);
    return;
  }

  // for
  if (node->kind == ND_FOR) {
    // [TODO] 無限ループ対応 (node->lhs->lhs->rhs is ND_EMP)
    int start_label = use_label();
    int end_label = use_label();

    gen_stmt(node->lhs->lhs->lhs); // 式文 or 空文
    println(".L%d:", start_label);
    gen(node->lhs->lhs->rhs);
    println("  pop rax");
    println("  cmp rax, 0");
    println("  je .L%d", end_label);
    gen_stmt(node->rhs); // 式文 or 空文
    gen_stmt(node->lhs->rhs);
    println("  jmp .L%d", start_label);
    println(".L%d:", end_label);
    return;
  }

  // 複文
  if (node->kind == ND_BLOCK) {
    for (int i = 0; i < node->stmt_num; i++) {
      Node* stmt = *(node->stmts + i);
      gen_stmt(stmt);
    }
    return;
  }

  // return文
  if (node->kind == ND_RETURN) {
    gen(node->lhs);
    println("  pop rax");
    println("  mov rsp, rbp");
    println("  pop rbp");
    println("  ret");
    return;
  }

  // 空文
  if (node->kind == ND_EMP) {
    return;
  }

  // 式文 (expr)
  gen(node);
  // 式の評価結果としてスタックに一つの値が残っているはずなので、スタックが溢れないようにポップしておく
  println("  pop rax");
  return;
}

void gen_funcdef(FuncDef* funcdef) {
  // 関数ラベル
  putchars(funcdef->name, funcdef->len);
  println(":");

  // プロローグ
  // 変数26個分の領域を確保する // TODO funcdef->localsの数 * 8 にする
  println("  push rbp");
  println("  mov rbp, rsp");
  println("  sub rsp, 208");
  // argument
  int argindex = 0;
  for (LocalVar* arg = funcdef->args; arg != NULL; arg = arg->next) {
    // ローカル変数に代入するのと同じ
    println("  mov rax, rbp");
    println("  sub rax, %d", arg->offset);
    println("  mov [rax], %s", argument_registers[argindex]);
  }

  // 各ステートメント
  for (int j = 0; funcdef->stmts[j]; j++) {
    gen_stmt(funcdef->stmts[j]);
  }

  // エピローグ
  // 最後の式の結果がRAXに残っているのでそれが返り値になる
  println("  mov rsp, rbp");
  println("  pop rbp");
  println("  ret");
}

void gen_main() {
  println(".intel_syntax noprefix");
  println(".global main");
  for (int i = 0; funcdefs[i]; i++) {
    gen_funcdef(funcdefs[i]);
  }
}
