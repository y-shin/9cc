#include "9cc.h"

char* user_input; // 入力プログラム
Token* token;  // (現在着目しているトークン)
FuncDef* funcdefs[100];
FuncDef* current_funcdef;
int label_id;

// エラー箇所を報告する
void error_at(char* loc, char* fmt, ...) {
  va_list ap;
  va_start(ap, fmt);

  int pos = loc - user_input;
  fprintf(stderr, "%s\n", user_input);
  fprintf(stderr, "%*s", pos, "");  // pos個の空白を出力
  fprintf(stderr, "^ ");
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
  exit(1);
}

// エラーを報告するための関数. printfと同じ引数を取る
void error(char* fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
  exit(1);
}

void logger(char* fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
}

void println(char* fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  printf("\n");
}

void putchars(char* str, unsigned int length) {
  for (unsigned int i = 0; i < length; i++) {
    putchar(*(str + i));
  }
}

int main(int argc, char** argv) {
  if (argc != 2) {
    error("引数の個数が正しくありません");
    return 1;
  }

  for (int i = 0; i < 100; i++) {
    funcdefs[i] = NULL;
  }
  current_funcdef = NULL;
  label_id = 0;
  user_input = argv[1]; // 入力を保存
  token = tokenize(argv[1]);  // トークナイズ
  program(); // 構文解析して結果をfuncdefsに入れる
  gen_main(); // アセンブラ出力
  return 0;
}
