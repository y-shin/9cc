#!/bin/bash
try() {
    id="$1"
    input="$2"
    echo "debug/$id.s -- $input"
    echo "# $input" > debug/$id.s
    echo "# ------------------" >> debug/$id.s
    ./9cc "$input" | tee tmp.s >> debug/$id.s
    gcc -o tmp tmp.s outer/outer.o
    ./tmp
    echo "$?"
    echo ""
}

mkdir -p debug

try 0 'x'
try 1 'int main(){4; if (0) {9;}}'
try 2 'int main(){{ 1; {2; {3;} 4; {5; }}}}'
try 3 'int main(){3;}'
try 4 'int main(){for(1;0;3){4;}}'
try 5 'int main(){for(;0;){}}'
try 6 'int main(){foo(); 98;}'
try 7 'int main(){bar(21, 4, 98, 32, 11, 55);}'
try 8 'int myfunc(){int x; x=4; x;} int main(){int x; x=2; x+myfunc();}'
try 9 'int myfunc(int x){int y;y=2; x+y;} int main(){int x; x=7; x+myfunc(3);}'
try 10 'int myfunc(int x){x;} int main(){int x;x=7;myfunc(x);}'
try 11 'int fib(int x){ if (x==0) { 1; } else if (x==1) { 1; } else { fib(x-1) + fib(x-2); } } int main(){fib(10);}'
try 12 'int sum(int x){ if (x==0) { 0; } else { x + sum(x-1); } } int main(){sum(18);}'
try 13 'int main(){ int x; x=15; x;}'
try 14 'int fib(int x){ if (x==0) { 1; } else if (x==1) { 1; } else { fib(x-1) + fib(x-2); } } int main(){print(fib(10));}'
try 15 'int main(){ int x; int y; int z; x=3; y=5; z=&y + 8; *z;}'
try 16 'int main(){ int x; x=3; *x;}'
try 17 'int main(){ int x; x=3; &x;}'
try 18 'int main(){ int x; x=3; }'
try 19 'int* main(){ int x; int *y; y=&x; *y=3; x; }'
try 20 'int* main(){ int x; int *y; int **z; y=&x; z=&y; **z=3; x; }'
