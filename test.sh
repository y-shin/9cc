#!/bin/bash
try() {
    expected="$1"
    input="$2"

    ./9cc "$input" > tmp.s
    gcc -o tmp tmp.s outer/outer.o
    ./tmp
    actual="$?"

    if [ "$actual" = "$expected" ]; then
        echo "$input => $actual"
    else
        echo "-------------------"
        cat tmp.s
        echo "$input => $expected expected, but got $actual"
        echo "-------------------"
        exit 1
    fi
}

compileerr() {
    input="$1"

    ./9cc "$input" > /dev/null
    if [ "$?" == 0 ]
    then
        exit 1
    else
        echo "$input => compile error"
    fi
}

try 0 'int main(){0;}'
try 42 'int main(){42;}'
try 21 'int main(){5+20-4;}'
try 41 'int main(){ 12 + 34 - 5 ; }'
try 47 'int main(){5+6*7;}'
try 15 'int main(){5*(9-6);}'
try 4 'int main(){(3+5)/2;}'
try 6 'int main(){9-2-1;}'
try 10 'int main(){-10+20;}'
try 20 'int main(){+10+10;}'
try 21 'int main(){+10++11;}'
try 8 'int main(){4*+2;}'
try 1 'int main(){9 == 9;}'
try 0 'int main(){9 == 99;}'
try 0 'int main(){8 != 8;}'
try 1 'int main(){8 != 88;}'
try 1 'int main(){3 > 2;}'
try 0 'int main(){3 > 3;}'
try 0 'int main(){2 > 3;}'
try 1 'int main(){3 >= 2;}'
try 1 'int main(){3 >= 3;}'
try 0 'int main(){2 >= 3;}'
try 0 'int main(){3 < 2;}'
try 0 'int main(){3 < 3;}'
try 1 'int main(){2 < 3;}'
try 0 'int main(){3 <= 2;}'
try 1 'int main(){3 <= 3;}'
try 1 'int main(){2 <= 3;}'
try 0 'int main(){-1 > 0;}'
try 1 'int main(){-1 > -2;}'
try 1 'int main(){-1 < 0;}'
try 0 'int main(){-1 < -2;}'
try 1 'int main(){-1 > -2 > 0;}'
try 0 'int main(){-2 < -1 < 1;}'
try 1 'int main(){0; 1;}'
try 9 'int main(){int c; c=9;}'
try 9 'int main(){int c; c=9;c;}'
try 19 'int main(){int c; int d; c=9;d=10;c+d;}'
try 3 'int main(){int a; int b; a=b=3;}'
try 9 'int main(){int a; int b; a=b=3; a*b;}'
try 8 'int main(){int abc; abc=8;}'
try 17 'int main(){int abc; abc=8; int xyz; xyz=9;abc+xyz;}'
try 4 'int main(){if(1) 4;}'
try 2 'int main(){int a; a=1; if(1)a=2; if(0)a=3; a;}'
try 3 'int main(){if(0) 4; else 3;}'
try 48 'int main(){int a; a=3; while(a<30) a=a+a; a;}'
try 2 'int main(){int a; a=2; for(;0;) a=10; a;}'
try 21 'int main(){int a; int i; a=0; for(i=5;i<10;i=i+2) a=a+i; a;}'
try 36 'int main(){int a; a=3; if (1) { a=a+a; a=a*a; } else { a=0; } a;}'
try 6 'int main(){{ 1; {2; {3;} 4; {5; }}} 6;}'
try 221 'int main(){bar(21, 4, 98, 32, 11, 55);}'
try 223 'int main(){int x; x=bar(21, 4, 98, 32, 11, 55); x+2;}'
try 89 'int fib(int x){ if (x==0) { 1; } else if (x==1) { 1; } else { fib(x-1) + fib(x-2); } } int main(){fib(10);}'
try 11 'int main(){ int x; int y; int z; x=11; y=17; z=&y + 8; *z;}'
try 3 'int main(){ int x; int y; x=3; y=&x; *y;}'
try 2 'int main(){ int ifa; ifa = 2;}'
compileerr 'int main() {int if; if=2;}'
compileerr 'int if() { 1; } int main() { 1; }'
try 11 'int main(){if (1){ return 11; } else { return 13; } 9; }'
try 11 'int main(){ return 11; return 13; }'
try 89 'int fib(int x){ if (x==0) { return 1; } else if (x==1) { return 1; } else { return fib(x-1) + fib(x-2); } } int main(){ return fib(10);}'
compileerr 'int main() {x=2;}'
compileerr 'int fun(int x, int x) { 1; } int main(){ 1; }'
compileerr 'int fun(int x) { int x; } int main(){ 1; }'
compileerr 'int main(){ 1 = 2; }'
try 11 'int main(){ int x; int *y; y=&x; *y=11; x; }'
try 11 'int main(){ int x; int *y; int **z; y=&x; z=&y; **z=11; x; }'
try 1 'int* main(){ 1; }'
try 1 'int** main(){ 1; }'
try 11 'int *fun(){ int x; int *y; y=&x; x=11; return y; } int main(){ int *z; z=fun(); return *z; }'
try 22 'int fun(int *x){ return *x * 2; } int main(){ int y; y=11; return fun(&y); }'
try 13 'int fun(int *x){ *x = *x + 2; return 0; } int main(){ int y; y=11; fun(&y); y; }'
try 13 'int fun(int *x){ *x = *x + 2; return 0; } int main(){ int y; int *z; y=11; z=&y; fun(z); y; }'

echo OK
