#include "9cc.h"

#define RESERVED_NUM 24
const char* reserveds[RESERVED_NUM] = {  // 文字数が多い順
  "return", "while", "else", "for", "int", "if", "==", "!=", ">=", "<=", "+", "-", "*", "/", ")", "(", "}", "{", ">", "<", ";", "=", ",", "&"
};

//////////////// tokenize /////////////////////////
Token* new_token(TokenKind kind, Token* cur, char* str, unsigned int len) {
  Token* tok = (Token*) calloc(1, sizeof(Token));
  tok->kind = kind;
  tok->str = str;
  tok->len = len;
  cur->next = tok;
  return tok;
}

void append_token(Token** cur, TokenKind kind, char* str, unsigned int len) {
  *cur = new_token(kind, *cur, str, len);
}

int reserved_length(char* p) {
  // lengthを返す. なければ-1
  for (int i = 0; i < RESERVED_NUM; i++) {  // RESERVED
    unsigned int len = strlen(reserveds[i]);
    if (memcmp(reserveds[i], p, len) == 0) {
      return len;
    }
  }
  return -1;
}

int ident_length(char* p) {
  // lengthをかえす. なければ-1
  // とりあえず英小文字の列 [a-z]+
  // 文字列が予約語と重複していた場合、-1を返す
  int len = 0;
  while (*(p + len) >= 'a' && *(p + len) <= 'z') {
    len++;
  }
  if (len > 0) {
    if (reserved_length(p) == len) {
      return -1;
    } else {
      return len;
    }
  } else {
    return -1;
  }
}

Token* tokenize(char* p) {
  Token head;
  head.next = NULL;
  Token* current_tail = &head;

  while (*p) {
    if (isspace(*p)) {  // 空白文字をスキップ
      p++;
    } else if (isdigit(*p)) {  // 数字トークン
      append_token(&current_tail, TK_NUM, p, 0);
      current_tail->val = strtol(p, &p, 10);
    } else if (ident_length(p) > 0) {  // 識別子トークン
      int len = ident_length(p);
      append_token(&current_tail, TK_IDENT, p, len);
      p += len;
    } else if (reserved_length(p) > 0) { // 予約語トークン
      int len = reserved_length(p);
      append_token(&current_tail, TK_RESERVED, p, len);
      p += len;
    } else {
      error("トークナイズできません");
    }
  }
  append_token(&current_tail, TK_EOF, p, 0);

  return head.next;
}

//////////////// use token /////////////////////////
bool is_reserved(char* rs) {
  if (token->kind != TK_RESERVED
      || strlen(rs) != token->len
      || memcmp(token->str, rs, token->len) != 0) {
    return false;
  }
  return true;
}

bool next_is_reserved(char* rs) {
  if (token->next) {
    // 1個先読みした後、元に戻す
    Token* current = token;
    token = token->next;
    bool result = is_reserved(rs);
    token = current;
    return result;
  } else {
    return false;
  }
}

bool consume_reserved(char* rs) {
  if (!is_reserved(rs)) {
    return false;
  }
  token = token->next;
  return true;
}

void expect_reserved(char* rs) {
  if (!is_reserved(rs)) {
    error("'%s'ではありません", rs);
  }
  token = token->next;
}

LocalVar* find_localvar(Token* tok) {
  // 現在着目している関数定義から、変数を名前で検索する. 見つからない場合はNULLを返す.
  for (LocalVar* current = current_funcdef->locals; current != NULL; current = current->next) {
    if (current->len == tok->len && memcmp(tok->str, current->name, tok->len) == 0) {
      return current;
    }
  }
  return NULL;
}

bool is_ident() {
  return (token->kind == TK_IDENT);
}

LocalVar* expect_localvar_use() {
  if (token->kind != TK_IDENT) {
    error("not ident");
  }

  LocalVar* lvar = find_localvar(token);
  if (lvar == NULL) {
    error("unknown local variable");
  }
  token = token->next;
  return lvar;
}

LocalVar* expect_localvar_def() {
  if (token->kind != TK_IDENT) {
    error("not ident");
  }

  LocalVar* lvar = find_localvar(token);
  if (lvar != NULL) {
    error("local variable is already defined");
  }
  // current_funcdef->localsの先頭に追加する
  lvar = (LocalVar*)calloc(1, sizeof(LocalVar));
  lvar->next = current_funcdef->locals;
  lvar->name = token->str;
  lvar->len = token->len;
  if (current_funcdef->locals == NULL) {
    lvar->offset = 8;
  } else {
    lvar->offset = current_funcdef->locals->offset + 8;
  }
  current_funcdef->locals = lvar;
  token = token->next;
  return lvar;
}

FuncCall* expect_funccall() {
  if (token->kind != TK_IDENT) {
    error("not ident");
  }
  FuncCall* funccall = (FuncCall*) calloc(1, sizeof(FuncCall));
  funccall->name = token->str;
  funccall->len = token->len;
  funccall->arg_num = 0;
  token = token->next;
  return funccall;
}

FuncDef* expect_funcdef() {
  if (token->kind != TK_IDENT) {
    error("not ident");
  }
  FuncDef* funcdef = (FuncDef*) calloc(1, sizeof(FuncDef));
  funcdef->name = token->str;
  funcdef->len = token->len;
  funcdef->locals = NULL;
  funcdef->args = NULL;
  token = token->next;
  return funcdef;
}

bool is_number() {
  return (token->kind == TK_NUM);
}

int expect_number() {
  if (!is_number()) {
    error_at(token->str, "数ではありません");
  }
  int val = token->val;
  token = token->next;
  return val;
}

bool at_eof() {
  return token->kind == TK_EOF;
}

//////////////// parser lib /////////////////////////
Node* new_node(NodeKind kind, Node* lhs, Node* rhs) {
  Node* node = (Node*) calloc(1, sizeof(Node));
  node->kind = kind;
  node->lhs = lhs;
  node->rhs = rhs;
  return node;
}

Node* new_node_localvar(LocalVar* lvar) {
  Node* node = (Node*) calloc(1, sizeof(Node));
  node->kind = ND_LOCALVAR;
  node->offset = lvar->offset;
  node->type = lvar->type;
  return node;
}

Node* new_node_funccall(FuncCall* funccall) {
  Node* node = (Node*) calloc(1, sizeof(Node));
  node->kind = ND_FUNCCALL;
  node->funccall_name = funccall->name;
  node->funccall_name_len = funccall->len;
  node->funccall_arg_num = funccall->arg_num;
  for (int i = 0; i < node->funccall_arg_num; i++) {
    node->funccall_args[i] = funccall->args[i];
  }
  return node;
}

Node* new_node_num(int val) {
  Node* node = (Node*) calloc(1, sizeof(Node));
  node->kind = ND_NUM;
  node->val = val;
  return node;
}

Node* new_node_emp() {
  Node* node = (Node*) calloc(1, sizeof(Node));
  node->kind = ND_EMP;
  return node;
}

Node* new_node_block() {
  Node* node = (Node*) calloc(1, sizeof(Node));
  node->kind = ND_BLOCK;
  node->stmts = (Node**) calloc(100, sizeof(Node*));
  return node;
}
/////////////////// grammar /////////////////////////
void program() {
  for (int i = 0; !at_eof(); i++) {
    funcdefs[i] = parse_funcdef();
  }
}

Type* type_declaration() {
  expect_reserved("int");
  Type* typ = (Type*) calloc(1, sizeof(Type));
  typ->kind = INT;
  typ->ptr_to = NULL;
  while (consume_reserved("*")) {
    Type* prev_typ = (Type*) calloc(1, sizeof(Type));
    prev_typ->kind = PTR;
    prev_typ->ptr_to = typ;
    typ = prev_typ;
  }
  return typ;
}

FuncDef* parse_funcdef() {
  Type* return_type = type_declaration();
  FuncDef* funcdef = expect_funcdef();
  funcdef->return_type = *return_type;
  current_funcdef = funcdef;
  // arguments
  expect_reserved("(");
  if (is_reserved("int")) {
    do {
      Type* arg_type = type_declaration();
      LocalVar* lvar = expect_localvar_def();
      lvar->type = *arg_type;
    } while (consume_reserved(","));
  }
  // この時点でのローカル変数は全てargumentである. これ以降にstatementで追加されるローカル変数は、現状のリストの先頭より前に追加される.
  funcdef->args = funcdef->locals;
  expect_reserved(")");

  // statement list
  expect_reserved("{");
  for (int i = 0; true; i++) {
    if (consume_reserved("}")) {
      break;
    }
    funcdef->stmts[i] = stmt();
  }
  return funcdef;
}

Node* stmt() {
  if (consume_reserved("if")) {
    // if文
    expect_reserved("(");
    Node* expr_node = expr();
    expect_reserved(")");
    Node* stmt_node = stmt();

    Node* if_node = new_node(ND_MID, expr_node, stmt_node);
    Node* else_node = new_node_emp();
    if (consume_reserved("else")) {
      else_node = stmt();
    }
    return new_node(ND_IF, if_node, else_node);
  } else if (consume_reserved("while")) {
    // while文
    expect_reserved("(");
    Node* expr_node = expr();
    expect_reserved(")");
    Node* stmt_node = stmt();
    return new_node(ND_WHILE, expr_node, stmt_node);
  } else if (consume_reserved("for")) {
    // for文
    expect_reserved("(");

    Node* initialize_node = NULL;
    if (is_reserved(";")) {
      initialize_node = new_node_emp();
      expect_reserved(";");
    } else {
      initialize_node = expr();
      expect_reserved(";");
    }

    Node* condition_node = NULL;
    if (is_reserved(";")) {
      condition_node = new_node_emp();
      expect_reserved(";");
    } else {
      condition_node = expr();
      expect_reserved(";");
    }

    Node* update_node = NULL;
    if (is_reserved(")")) {
      update_node = new_node_emp();
      expect_reserved(")");
    } else {
      update_node = expr();
      expect_reserved(")");
    }

    Node* stmt_node = stmt();

    // initialize: node->lhs->lhs->lhs
    // condition:  node->lhs->lhs->rhs
    // update:     node->lhs->rhs
    // stmt:       node->rhs
    return new_node(
             ND_FOR,
             new_node(ND_MID,
                      new_node(ND_MID,
                               initialize_node,
                               condition_node),
                      update_node),
             stmt_node);
  } else if (consume_reserved("{")) {
    // 複文
    int i = 0;
    Node* block_node = new_node_block();
    while (!consume_reserved("}")) {
      *(block_node->stmts + i) = stmt();
      i++;
    }
    block_node->stmt_num = i;
    return block_node;
  } else if (consume_reserved("return")) {
    // return文
    Node* expr_node = expr();
    expect_reserved(";");
    return new_node(ND_RETURN, expr_node, NULL);
  } else if (is_reserved("int")) {
    // 変数宣言文
    Type* var_type = type_declaration();
    LocalVar* lvar = expect_localvar_def();
    lvar->type = *var_type;
    expect_reserved(";");
    return new_node_emp();
  } else {
    // 式文
    Node* node = expr();
    expect_reserved(";");
    return node;
  }
}

Node* expr() {
  return assign();
}

Node* assign() {
  Node* node = equality();
  if (consume_reserved("=")) {
    node = new_node(ND_ASSIGN, node, assign());
  }
  return node;
}

Node* equality() {
  Node* node = relational();
  while (true) {
    if (consume_reserved("==")) {
      node = new_node(ND_EQ, node, relational());
    } else if (consume_reserved("!=")) {
      node = new_node(ND_NE, node, relational());
    } else {
      return node;
    }
  }
}

Node* relational() {
  Node* node = add();
  while (true) {
    if (consume_reserved("<")) {
      node = new_node(ND_LT, node, add());
    } else if (consume_reserved("<=")) {
      node = new_node(ND_LTE, node, add());
    } else if (consume_reserved(">")) {
      node = new_node(ND_LT,  add(), node);
    } else if (consume_reserved(">=")) {
      node = new_node(ND_LTE, add(), node);
    } else {
      return node;
    }
  }
}

Node* add() {
  Node* node = mul();
  while (true) {
    if (consume_reserved("+")) {
      node = new_node(ND_ADD, node, mul());
    } else if (consume_reserved("-")) {
      node = new_node(ND_SUB, node, mul());
    } else {
      return node;
    }
  }
}

Node* mul() {
  Node* node = unary();
  while (true) {
    if (consume_reserved("*")) {
      node = new_node(ND_MUL, node, unary());
    } else if (consume_reserved("/")) {
      node = new_node(ND_DIV, node, unary());
    } else {
      return node;
    }
  }
}

Node* unary() {
  if (consume_reserved("+")) {
    return primary();
  } else if (consume_reserved("-")) {
    return new_node(ND_SUB, new_node_num(0), primary());
  } else if (consume_reserved("&")) {
    return new_node(ND_ADDR, unary(), NULL);
  } else if (consume_reserved("*")) {
    return new_node(ND_DEREF, unary(), NULL);
  } else {
    return primary();
  }
}

Node* primary() {
  if (consume_reserved("(")) {
    Node* node = expr();
    expect_reserved(")");
    return node;
  } else if (is_ident()) {
    if (next_is_reserved("(")) {
      FuncCall* funccall = expect_funccall();
      expect_reserved("(");
      if (!is_reserved(")")) {
        do {
          funccall->args[funccall->arg_num] = expr(); //[TODO]リスト構造にする
          funccall->arg_num++;
        } while (consume_reserved(","));
      }
      expect_reserved(")");
      return new_node_funccall(funccall);
    } else {
      return new_node_localvar(expect_localvar_use());
    }
  } else {
    return new_node_num(expect_number());
  }
}
