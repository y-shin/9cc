#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma GCC diagnostic ignored "-Wwrite-strings"

///////// typedef ///////
// トークンの種類
typedef enum {
  TK_RESERVED, // 記号
  TK_IDENT,    // 識別子
  TK_NUM,      // 整数トークン
  TK_EOF,      // 入力の終わりを表すトークン
} TokenKind;

// トークン型
typedef struct Token Token;
struct Token {
  TokenKind kind; // トークンの型
  Token* next;    // 次の入力トークン
  int val;        // kindがTK_NUMの場合、その数値
  char* str;      // トークン文字列
  unsigned int len;        // トークン長さ
};

// 抽象構文木のノードの種類
typedef enum {
  ND_MID, // 仮想中間ノード (子が3つ以上ほしいとき)
  ND_EMP, // 空文
  ND_ADD, // +
  ND_SUB, // -
  ND_MUL, // *
  ND_DIV, // /
  ND_EQ, // ==
  ND_NE, // !=
  ND_LT, // <
  ND_LTE, // <=
  ND_EXPR, // EXPR
  ND_ASSIGN, // =
  ND_LOCALVAR, // ローカル変数
  ND_NUM, // 整数
  ND_IF, // if
  ND_WHILE, // while
  ND_FOR, // for
  ND_BLOCK, // 複文
  ND_FUNCCALL, // 関数呼び出し
  ND_ADDR,  // 単項&
  ND_DEREF,  // 単項*
  ND_RETURN,  // return
} NodeKind;

// 型定義
typedef enum { INT, PTR } TypeKind;
typedef struct Type Type;
struct Type {
  TypeKind kind;
  Type* ptr_to;
};

// ローカル変数
typedef struct LocalVar LocalVar;
struct LocalVar {
  LocalVar* next;   // 次の変数 or NULL
  char* name;       // 変数名
  unsigned int len; // 変数の長さ
  int offset;       // RBPからのオフセット
  Type type;        // 型
} ;

// 抽象構文木のノードの型
typedef struct Node Node;
struct Node {
  NodeKind kind; // ノードの型
  Node* lhs;     // 左辺
  Node* rhs;     // 右辺
  int val;       // 数値 (kind=ND_NUM)
  int offset;    // オフセット (kind=ND_LOCALVAR)
  Type type;     // 型        (kind=ND_LOCALVAR)
  Node** stmts;  // 文リスト (kind=ND_BLOCK)
  int stmt_num;  // 文リスト長さ (kind=ND_BLOCK)
  char* funccall_name;                // 関数名(KIND=ND_FUNCCALL)
  unsigned int funccall_name_len; // 関数名長さ(KIND=ND_FUNCCALL)
  Node* funccall_args[6];               // 関数の引数(KIND=ND_FUNCCALL)
  unsigned int funccall_arg_num;      // 関数の引数長さ(KIND=ND_FUNCCALL)
};

// 関数定義ノードの型 (Nodeとはとりあえず別にする [TODO]Nodeにするかも)
typedef struct FuncDef FuncDef;
struct FuncDef {
  char* name;       // 関数名
  unsigned int len; // 関数名の長さ
  LocalVar* args;  // 引数
  LocalVar* locals;  // ローカル変数
  Node* stmts[100]; // node list
  Type return_type;  // 返り値の型
};

// 関数呼び出し
typedef struct FuncCall FuncCall;
struct FuncCall {
  char* name;       // 関数名
  unsigned int len; // 関数名の長さ
  Node* args[6];          // 引数
  unsigned int arg_num;  // 引数の個数
};

/////// global variables  ///////////
extern char* user_input; // 入力プログラム
extern Token* token;  // (現在着目しているトークン)
extern FuncDef* current_funcdef;  // (現在着目している関数定義)
extern FuncDef* funcdefs[100]; // func definition list
extern int label_id; // ラベル番号

//////// functions /////////////
// utilities
void error_at(char* loc, char* fmt, ...);
void error(char* fmt, ...);
void logger(char* fmt, ...);
void println(char* fmt, ...);
void putchars(char* str, unsigned int length);
// tokenize
Token* tokenize(char* p);
// grammar
void program();
Type* type_declaration();
FuncDef* parse_funcdef();
Node* stmt();
Node* expr();
Node* assign();
Node* equality();
Node* relational();
Node* add();
Node* mul();
Node* unary();
Node* primary();
// code generator
void gen_main();
