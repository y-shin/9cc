CFLAGS=-std=c11 -g -static
SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)

9cc: $(OBJS)
	$(CC) -o 9cc $(OBJS) $(LDFLAGS)

$(OBJS): 9cc.h

outer/outer.o: outer/outer.c
	$(CC) -c -o outer/outer.o outer/outer.c

test: 9cc outer/outer.o
	./test.sh

debug: 9cc outer/outer.o
	./rundebug.sh

clean:
	rm -f 9cc *.o *~ tmp* outer/*.o
	rm -rf debug/

astyle:
	for f in *.c; do astyle -A14 -s2 -p -H -U -k1 -z2 < $$f > tmp.$$f.x; cp -f tmp.$$f.x $$f ; done
	for f in *.h; do astyle -A14 -s2 -p -H -U -k1 -z2 < $$f > tmp.$$f.y; cp -f tmp.$$f.y $$f ; done

.PHONY: test debug clean astyle outer
