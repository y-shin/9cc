#include <stdio.h>
int foo() {
  printf("this is foo function\n");
  return 0;
}
int bar(int a, int b, int c, int d, int e, int f) {
  printf("this is bar function / %d %d %d %d %d %d\n", a, b, c, d, e, f);
  return a + b + c + d + e + f;
}
int print(int a) {
  printf("%d\n", a);
  return 0;
}
